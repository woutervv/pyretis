.. _exercises-index:


|pyretis| exercises
===================

Here, we list some exercises in which |pyretis| has been used
to demonstrate some different simulation methods.

.. toctree::
    :maxdepth: 2

    examples-molmod-2016.rst
    examples-cecam-2016.rst
