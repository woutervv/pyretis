
.. _api-bin:

pyretis.bin
===========

Here, the |pyretis| executables can be found. These are:

.. contents:: :local:

.. _api-bin-pyretisrun:

pyretis.bin.pyretisrun module
-----------------------------

.. automodule:: pyretis.bin.pyretisrun
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-bin-pyretisanalyse:

pyretis.bin.pyretisanalyse module
---------------------------------

.. automodule:: pyretis.bin.pyretisanalyse
    :members:
    :undoc-members:
    :show-inheritance:
