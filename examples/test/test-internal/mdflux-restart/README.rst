MD-Flux restart
===============

This folder test the restart function when running a MD-flux
simulation. There are two versions of the test.

Version1
--------

Here, a plain MD-flux simulation is performed using only one
interface.

Version2
--------

Here, the MD-flux simulation is performed with several interfaces.
Further, the simulation is set up so that the restart happens exactly
at a step where a crossing is observed.
